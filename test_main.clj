(load-file "main.clj")

(assert (= 4 (max-min-segment-space 2 [8 2 4 6])))
(assert (= 2 (max-min-segment-space 4 [8 2 4 6])))
(assert (= 2 (max-min-segment-space 4 [8 2 4])))
(assert (= nil (max-min-segment-space 1 [8 2 4])))
