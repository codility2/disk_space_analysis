(defn max-min-segment-space[n-segment spaces]
  (if (> n-segment 1)
    (let [n (min n-segment (count spaces))]
      (->> (partition n 1 spaces)
           (map #(apply min %))
           (apply max)))))
